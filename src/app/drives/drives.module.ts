
import { NgModule }                       from '@angular/core';
import { CommonModule }                   from '@angular/common';

import { MyDrivesComponent }              from './my-drives/my-drives.component';
import { CompletedDrivesComponent }       from './completed-drives/completed-drives.component';
import { DrivesComponent }                from './drives.component';
import { DrivesRoutingModule }            from './drives-route.module';
import { DrivesService }                  from './drives.service';
import { ReaddrivesComponent }            from './readdrives/readdrives.component';

@NgModule({
  declarations: [
    MyDrivesComponent,
    CompletedDrivesComponent,
    DrivesComponent,
    ReaddrivesComponent,
 
  ],
  imports: [
    CommonModule,
    DrivesRoutingModule
  ],
  exports:[
    DrivesComponent
  ],
  providers: [DrivesService],
})

export class DrivesModule { }
