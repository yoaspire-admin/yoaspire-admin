/*
*Service class component  for displaying all the drives of the user by the admin
*/
import { Injectable }                         from '@angular/core';
import { HttpClient,HttpHeaders }             from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class DrivesService {

  httpOptions: {};
  constructor(private http: HttpClient) {}

  doComplitedDrives()
  {
    let token=localStorage.getItem('jwt-token');

       this.httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'Authorization': 'Bearer '+token
        })
      }

      return this.http.get<any>('api/drives/previous',this.httpOptions);
    }
    
  doFetchDrives()
  {
    let token=localStorage.getItem('jwt-token');

       this.httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'Authorization': 'Bearer '+token
        })
      }

      return this.http.get<any>('api/drives',this.httpOptions);
    }
  
    readDriveById( id:any)
    {
      
      let token=localStorage.getItem('jwt-token');
      
         this.httpOptions = {
          headers: new HttpHeaders({
            'Content-Type':  'application/json',
            'Authorization': 'Bearer '+token
          })
        }
  
        return this.http.get<any>('api/drives/'+id,this.httpOptions);
      }
}
