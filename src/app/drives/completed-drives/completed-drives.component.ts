 /*
 *  Author         :Ramu
    Date           :15-03-2019
    Description    : service class for displaying all the completed drives of the user
*/

import { Component, OnInit }             from '@angular/core';
import {DrivesService }                   from '../drives.service';

@Component({
  selector: 'app-completed-drives',
  templateUrl: './completed-drives.component.html',
  styleUrls: ['./completed-drives.component.css']
})

export class CompletedDrivesComponent implements OnInit {

  constructor( private service:DrivesService) { }
  complitedDrives:any;
  ngOnInit() {
    this.service.doComplitedDrives().subscribe(
      (data)=>{
       
        if(data.statuscode == 1)
        {
          console.log(data.drives)
          this.complitedDrives=data.drives         
        }
      
      }
    )
  }

}
