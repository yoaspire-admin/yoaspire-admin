/*
 *   Author        : Ramu
     Date        : 16-03-2019
     Description :service component class displaying all the drives of the user
*/
     
import { Component, OnInit }          from '@angular/core';

@Component({
  selector: 'app-drives',
  templateUrl: './drives.component.html',
  styleUrls: ['./drives.component.css']
})

export class DrivesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
