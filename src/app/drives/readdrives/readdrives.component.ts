/*
 * Author           :Ramu
   Date             :15-03-2019
   Description      :service class component for displaying all the complete details pf the drive 
                     like venue,date,qualification..etc
 */
import { Component, OnInit } from '@angular/core';
import {DrivesService } from '../drives.service';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'app-readdrives',
  templateUrl: './readdrives.component.html',
  styleUrls: ['./readdrives.component.css']
})
export class ReaddrivesComponent implements OnInit {

 
  constructor( private service:DrivesService,private activatedRoute: ActivatedRoute) { }
  drives:any;
  id:any;
  img:string='http://13.250.235.137:8050/public/images/';
  imgUrl:any;
  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.id = params['id'];
       
      this.service.readDriveById(this.id).subscribe(
        (data)=>{
        
          if(data.statuscode == 1)
          {
            this.drives=data.drive;
            this.imgUrl=this.img+this.drives.image;
            console.log("sdfsdf"+this.imgUrl);
          }
        
        }
      )

  });
   
  }

}
