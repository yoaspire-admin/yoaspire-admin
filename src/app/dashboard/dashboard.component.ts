// Dashboard component

import { Component, OnInit }        from '@angular/core';
import { ServiceService }           from '../service.service';
import {Router}                     from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {
  header:boolean;
  constructor(private service:ServiceService,private router: Router,) { }
  ngOnInit() {
    // this.service.dashboardNavbar();     
    let myDate = new Date();
    console.log(myDate);
  }
  public pieChartLabels:string[] = ["Employees", "Interviews", "Recruiters", "Candites"];
  public pieChartData:number[] = [21, 39, 10, 14];
  public pieChartType:string = 'pie';
  public pieChartOptions:any = {'backgroundColor': [
               "#FF6384",
            "#4BC0C0",
            "#FFCE56",
            "#E7E9ED",
            "#36A2EB"
            ]}
 
  // events on slice click
  public chartClicked(e:any):void {
    console.log(e);
  }
 
 // event on pie chart slice hover
  public chartHovered(e:any):void {
    console.log(e);
  }
}
