/**
 * ng modules
 */
import { NgModule }                       from '@angular/core';
import { CommonModule }                   from '@angular/common';

import { BulletinsComponent }             from './bulletins.component';
import { BullietinsRouteModule }          from './bulletins-route.module';

@NgModule({
  declarations: [
    BulletinsComponent,
  ],
  imports: [
    CommonModule,
    BullietinsRouteModule
  ],
  exports:[
    
  ]

})
export class BulletinsModule { }
