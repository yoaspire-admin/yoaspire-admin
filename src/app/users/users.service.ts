import { Injectable }                       from '@angular/core';
import { HttpClient,HttpHeaders }           from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class UsersService {
  
  httpOptions: {};
  constructor(private http: HttpClient) {
  }

  searchUser(search_string: any) {
    let token=localStorage.getItem('jwt-token');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token
      })
    };
    let data = {
      search_string: search_string
    };
    return this.http.post<any>('http://13.250.235.137:8050/api/user/search', data, this.httpOptions);
  }

  readUser(id: number) {
    let token=localStorage.getItem('jwt-token');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token
      })
    };
    return this.http.get<any>('http://13.250.235.137:8050/api/user/' + id, this.httpOptions);
  }

  userFriends(id: number) {
    let token=localStorage.getItem('jwt-token');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token
      })
    };
    return this.http.get<any>('http://13.250.235.137:8050/api/user/' + id, this.httpOptions);
  }
  
}
