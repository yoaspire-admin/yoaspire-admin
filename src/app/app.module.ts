/**
 * npm modules
 */
import { BrowserModule }                         from '@angular/platform-browser';
import { NgModule }                              from '@angular/core';
import { FormsModule, ReactiveFormsModule }      from '@angular/forms';
import { HttpClientModule }                      from '@angular/common/http';
import { ChartsModule }                          from 'ng2-charts';

/**
 * app routing module
 */
import { AppRoutingModule }                      from './app-routing.module';

/**
 * modules of components
 */
import { ArticlesModule }                        from './articles/articles.module';
import { UsersModule }                           from './users/users.module';
import { DrivesModule }                          from './drives/drives.module';
import { BulletinsModule }                       from './bulletins/bulletins.module';
import { ComplaintsModule }                      from './complaints/complaints.module';

/**
 * General componets authentication,header and navigations.
 */
import { AppComponent }                          from './app.component';
import { LoginComponent }                        from './login/login.component';
import { HeaderComponent }                       from './header/header.component';
import { NavbarComponent }                       from './navbar/navbar.component';
import { DashboardComponent }                    from './dashboard/dashboard.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    NavbarComponent,
    DashboardComponent,   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ArticlesModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    UsersModule,
    DrivesModule,
    BulletinsModule,
    ComplaintsModule,
    ChartsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
