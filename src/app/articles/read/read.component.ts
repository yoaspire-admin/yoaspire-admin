/*
*  Author : Rithika
   Date : 16-03-2019
   Description : This component is used for reading complete details of  article hitting the read article service  
 **/
import { Component, OnInit }                from '@angular/core';
import { Router, ActivatedRoute, Params }   from '@angular/router';
import { ArticlesService }                  from '../articles.service';

@Component({
  selector: 'app-read',
  templateUrl: './read.component.html',
  styleUrls: ['./read.component.css']
})

export class ReadComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute, private service: ArticlesService) { }
  id: number;
  articleData: any; 
  
  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.id = params['id'];
      console.log("ID", this.id);
    debugger;
    this.service.readArticle(this.id)
      .subscribe((data: any) => {
        debugger;
        this.articleData = data.article;
        console.log(this.articleData);
      });
    })
  }

}
